# Copyright 2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde test-dbus-daemon

SUMMARY="Library providing a job-based API for interacting with an IMAP4 server"
DESCRIPTION="
It manages connections, encryption and parameter quoting and encoding, but
otherwise provides quite a low-level interface to the protocol.  This library
does not implement an IMAP client; it merely makes it easier to do so."

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    GPL-2 LGPL-2.1
"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER="5.19.0"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmime:5[>=16.03.40] [[ note = [ aka 5.1.40 ] ]]
        net-libs/cyrus-sasl[>=2]
        x11-libs/qtbase:5[>=5.2.0][?gui]
"

# TODO: loginjobtest fails if you don't have support for SSLv3
RESTRICT="test"

