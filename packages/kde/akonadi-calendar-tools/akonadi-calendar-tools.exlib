# Copyright 2016 Niels Ole Salscheider <olesalscheider@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="A command line interface to KDE calendars"
DESCRIPTION="
It lets you view, insert, remove, or modify calendar events by way of the
command line or from a scripting language. Additionally, konsolekalendar can
create a new KDE calendar, export a KDE calendar to a variety of other
formats, and import another KDE calendar."

LICENCES="FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER=5.44.0
QT_MIN_VER=5.8.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde/calendarsupport[>=${PV}]
        kde/libkdepim[>=${PV}]
        kde-frameworks/akonadi-calendar:5[>=${PV}]
        kde-frameworks/kcalcore:5[>=${PV}]
        kde-frameworks/kcalutils:5[>=${PV}]
        kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        !kde/kdepim [[
            description = [ akonadi-calendar-tools has been split out from kdepim ]
            resolution = uninstall-blocked-after
        ]]
"

akonadi-calendar-tools_pkg_postinst() {
    gtk-icon-cache_pkg_postinst
}

akonadi-calendar-tools_pkg_postrm() {
    gtk-icon-cache_pkg_postrm
}

