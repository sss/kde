# Copyright 2011,2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdegraphics.exlib', which is:
#     Copyright 2008 Bernd Steinhauser and 2008-2011 Bo Ørsted Andresen

require kde-apps kde [ translations='ki18n' ] freedesktop-desktop gtk-icon-cache

export_exlib_phases src_configure pkg_postinst pkg_postrm

SUMMARY="A fast and easy to use image viewer by KDE"

LICENCES="FDL-1.2 GPL-2"
MYOPTIONS="
    fits [[ description = [ Support for the FITS format often used in astronomy ] ]]
    kipi [[ description = [ Provides various image manipulation and export features ] ]]
    raw  [[ description = [ Enable reading RAW image files from digital photo cameras ] ]]
    semantic-desktop [[ description = [ Use baloo's semantic info backend ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

KF5_MIN_VER=5.25.0
QT_MIN_VER=5.6.0

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        graphics/exiv2[>=0.19]
        media-libs/lcms2
        media-libs/libpng:=
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemmodels:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        media-libs/phonon[qt5]
        x11-libs/libX11
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        fits? ( sci-libs/cfitsio )
        kipi? ( kde/libkipi:5 )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        raw? ( kde-frameworks/libkdcraw:5 )
        semantic-desktop? (
            kde-frameworks/baloo:5[>=5.1.90]
            kde-frameworks/kfilemetadata:5[>=5.1.90]
        )
    run:
        kipi? ( graphics/kipi-plugins[>=5.0.0-beta] )
    suggestion:
        kde-frameworks/kimageformats:5 [[
            description = [ View eps, exr, psd and other file formats supported by kimageformats ]
        ]]
        x11-libs/qtimageformats:5 [[
            description = [ View tiff, mng, tga and other file formats supported by qtimageformats ]
        ]]
"

# 8 of 15 tests need a running X server
RESTRICT="test"

gwenview_src_configure() {
    local cmakeparams=()

    cmakeparams+=(
        $(cmake_disable_find fits CFitsio)
        $(cmake_disable_find kipi KF5Kipi)
        $(cmake_disable_find raw KF5KDcraw)
    )

    if option semantic-desktop ; then
        cmakeparams+=( -DGWENVIEW_SEMANTICINFO_BACKEND=Baloo )
    else
        cmakeparams+=( -DGWENVIEW_SEMANTICINFO_BACKEND=None )
    fi

    ecmake \
        "${CMAKE_SRC_CONFIGURE_PARAMS[@]}" \
        "${CMAKE_SRC_CONFIGURE_TESTS[@]}" \
        "${cmakeparams[@]}"
}

gwenview_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

gwenview_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

