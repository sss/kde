# Copyright 2008, 2009, 2010, 2011, 2012 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2010-2011 Bo Ørsted Andresen
# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

if ever at_least 5.12.90 ; then
    require option-renames [ renames=[ 'dictionary qtwebengine' ] ]
fi
require plasma kde [ translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Addons for plasma"
DESCRIPTION="
KDE Plasmoids module is a collection of additional Plasma data engines and widgets (applets).
"

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] GPL-2 LGPL-2.1"
SLOT="4"

if ever at_least 5.12.90 ; then
    MYOPTIONS="
        qtwebengine [[ description = [ Build a dictionary and a web browser applet ] ]]
    "
    KF5_MIN_VER=5.45.0
else
    MYOPTIONS="
        dictionary [[ description = [ Build a dictionary applet ] ]]
    "

    KF5_MIN_VER=5.42.0
fi
QT_MIN_VER=5.9.0

DEPENDENCIES+="
    build+run:
        kde/plasma-workspace:4[>=5.5.90]   [[ note = [ LibTaskManager ] ]]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}] [[ note = [ quickshare applet ] ]]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/kross:5[>=${KF5_MIN_VER}]
        kde-frameworks/krunner:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kunitconversion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/plasma-framework:5[>=${KF5_MIN_VER}]
        kde-frameworks/sonnet:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}] [[ note = [ quickshare applet ] ]]
    run:
        x11-libs/qtgraphicaleffects:5
    suggestion:
        kde-frameworks/purpose:5 [[
            description = [ Runtime dependency for the experimental QuickShare applet ]
        ]]
"

if ever at_least 5.12.90 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kholidays:5[>=${KF5_MIN_VER}]
            qtwebengine? ( x11-libs/qtwebengine:5[>=${QT_MIN_VER}] )
    "

    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        'qtwebengine Qt5WebEngine'
    )
else
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
            kde-frameworks/kdelibs4support:5[>=${KF5_MIN_VER}]
            kde-frameworks/solid:5[>=${KF5_MIN_VER}]
            x11-libs/libX11
            x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
            dictionary? ( x11-libs/qtwebengine:5[>=${QT_MIN_VER}] )
    "

    CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
        'dictionary Qt5WebEngine'
    )
fi

kdeplasma-addons_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kdeplasma-addons_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

