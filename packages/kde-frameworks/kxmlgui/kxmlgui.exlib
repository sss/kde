# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks kde [ translations='ki18n' ]

SUMMARY="Framework for managing menu and toolbar actions"
DESCRIPTION="
libkxmlgui provides a framework for managing menu and toolbar actions in an
abstract way. The actions are configured through a XML description and hooks
in the application code. The framework supports merging of multiple
description for example for integrating actions from plugins."

LICENCES="GPL-2 LGPL-2.1"
MYOPTIONS="
    attica [[ description = [ Support for Get How New Stuff ] ]]
"

DEPENDENCIES="
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kglobalaccel:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        attica? ( kde-frameworks/attica:5[>=${KF5_MIN_VER}] )
    suggestion:
        (
            kde/kde-cli-tools:4
            kde/user-manager:4
        ) [[ *description = [ Required to set an E-Mail address for bug reports ] ]]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'attica KF5Attica' )

# 5 of 5 tests need a running X server (5.0.0)
RESTRICT="test"

