# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks [ docs=false ] kde

SUMMARY="Integration of Qt application with KDE work spaces"
DESCRIPTION="
A set of plugins responsible for better integration of Qt applications when
running on a KDE Plasma workspace.
Applications do not need to link to this directly."

LICENCES="LGPL-2.1"
MYOPTIONS="
    X [[ presumed = true ]]
"

DEPENDENCIES="
    build:
        virtual/pkg-config [[ note = [ needed for finding libxcb ] ]]
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        X? (
            x11-libs/libxcb
            x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        )
"

# 1 of 1 test needs a running X server (5.22.0)
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DCMAKE_DISABLE_FIND_PACKAGE_packagekitqt5:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_AppStreamQt:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'X XCB' )

